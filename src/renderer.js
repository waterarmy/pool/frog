document.addEventListener('DOMContentLoaded', () => {
  const ipc = window.ipc
  const fullScreenShot = window.fullScreenShot
  const panelsMap = new Map()

  ipc.on('do-stdout', (e, data) => {
    const { id, command, out } = data
    console.log(`>> 命令 ${command} 执行日志：\n`, out)
  })

  ipc.on('done', (e, data) => {
    const { id, command, code } = data
    console.log(`>> 命令 ${command} 执行完毕：`, code)
    const panel = panelsMap.get(id)
    if (panel) {
      panel.do_btn.style.display = ''
      panel.undo_btn.style.display = 'none'
    }
  })

  // panel1
  const panel1 = document.querySelector('#panel1')
  const panel1_btns = panel1.querySelectorAll('button').values()
  const panel1_do_btn = panel1_btns.next().value
  const panel1_undo_btn = panel1_btns.next().value
  const panel1_input = panel1.querySelector('input')
  const panel1_spans = panel1.querySelectorAll('span')

  panel1_do_btn.addEventListener('click', (e) => {
    const spans = panel1_spans.values()
    const prefix = spans.next().value.innerText
    const suffix = spans.next().value.innerText
    const input = panel1_input.value
    const command = `${prefix}${input} ${suffix}`
    console.log(command)
    ipc.send('do', {
      id: 1,
      command,
    })
    e.target.style.display = 'none'
    panel1_undo_btn.style.display = ''
  })
  panel1_undo_btn.style.display = 'none'
  panel1_undo_btn.addEventListener('click', (e) => {
    ipc.send('undo', {
      id: 1,
    })
  })
  panelsMap.set(1, {
    do_btn: panel1_do_btn,
    undo_btn: panel1_undo_btn,
  })

  // panel2
  const panel2 = document.querySelector('#panel2')
  const panel2_btns = panel2.querySelectorAll('button').values()
  const panel2_do_btn = panel2_btns.next().value
  const panel2_undo_btn = panel2_btns.next().value
  const panel2_input = panel2.querySelector('input')
  const panel2_spans = panel2.querySelectorAll('span')

  panel2_do_btn.addEventListener('click', (e) => {
    const spans = panel2_spans.values()
    const prefix = spans.next().value.innerText
    const suffix = spans.next().value.innerText
    const input = panel2_input.value
    const command = `${prefix} ${input} ${suffix}`
    console.log(command)
    ipc.send('do', {
      id: 2,
      command,
    })
    e.target.style.display = 'none'
    panel2_undo_btn.style.display = ''
  })
  panel2_undo_btn.style.display = 'none'
  panel2_undo_btn.addEventListener('click', (e) => {
    ipc.send('undo', {
      id: 2,
    })
  })
  panelsMap.set(2, {
    do_btn: panel2_do_btn,
    undo_btn: panel2_undo_btn,
  })

  // panel3
  const panel3 = document.querySelector('#panel3')
  const panel3_btns = panel3.querySelectorAll('button').values()
  const panel3_do_btn = panel3_btns.next().value
  const panel3_undo_btn = panel3_btns.next().value
  const panel3_span = panel3.querySelector('span')

  panel3_do_btn.addEventListener('click', (e) => {
    const command = panel3_span.innerText
    console.log(command)
    ipc.send('do', {
      id: 3,
      command,
    })
    e.target.style.display = 'none'
    panel3_undo_btn.style.display = ''
  })
  panel3_undo_btn.style.display = 'none'
  panel3_undo_btn.addEventListener('click', (e) => {
    ipc.send('undo', {
      id: 3,
    })
  })
  panelsMap.set(3, {
    do_btn: panel3_do_btn,
    undo_btn: panel3_undo_btn,
  })

  // panel4
  const panel4 = document.querySelector('#panel4')
  const panel4_btn = panel4.querySelector('button')
  panel4_btn.addEventListener('click', async (e) => {
    const imageInfo = await fullScreenShot()
    if (imageInfo) {
      const { file } = imageInfo
      console.log(`>> 截屏已经保存到：\n`, file)
    }
  })
})
