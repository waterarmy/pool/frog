const { app } = require('electron')
const fs = require('fs')
const path = require('path')
const Url = require('url')
const fse = require('fs-extra')
const randomString = require('random-string-generator')

const getDataPath = (subPath) => path.join(app.getPath('appData'), app.name, subPath)

/**
 * Decode base64 string to buffer.
 *
 * @param {String} base64Str string
 * @return {Object} Image object with image type and data buffer.
 * @public
 */
function decodeBase64Image(base64Str) {
  // eslint-disable-next-line
  const matches = base64Str.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/)
  const image = {}
  if (!matches || matches.length !== 3) {
    throw new Error('Invalid base64 string')
  }
  image.type = matches[1]
  image.data = Buffer.from(matches[2], 'base64')
  return image
}

/**
 * Change base64Str to file.
 *
 * @param {String} abs File's absolute path.
 * * @param {Object} bufferData Buffer
 * @return {Promise} The result.
 * @public
 */
function base64ToFile(abs, bufferData) {
  return new Promise((resolve, reject) => {
    fs.writeFile(abs, bufferData, 'base64', (error) => {
      if (error) {
        reject(error)
      }
      resolve()
    })
  })
}

/**
 * Change base64Str to image and write image file with the specified file name to the specified file path.
 *
 * @param {String} base64 string (mandatory)
 * @param {String} file path e.g. /opt/temp/uploads/ (mandatory)
 * @return {Object} optionsObj holds image type, image filename, debug e.g.{'fileName':fileName, 'type':type, 'debug':true} (optional)
 * @public
 */
async function base64ToImage(base64Str, filePath, optionalObj) {
  if (!base64Str || !filePath) {
    throw new Error('Missing mandatory arguments base64 string and/or path string')
  }

  optionalObj = optionalObj || {}
  const imageBuffer = decodeBase64Image(base64Str)
  let imageType = optionalObj.type || imageBuffer.type || 'png'
  let fileName = optionalObj.fileName || `img-${Date.now()}${randomString(4, 'numeric')}`

  if (fileName.indexOf('.') === -1) {
    imageType = imageType.replace('image/', '')
    fileName = `${fileName}.${imageType}`
  }

  if (!(await fse.pathExists(filePath))) {
    await fse.ensureDir(filePath)
  }

  const abs = path.join(filePath, fileName)

  await base64ToFile(abs, imageBuffer.data)
  return {
    file: abs,
    fileName,
    filePath,
    imageType,
  }
}

// 将 base64 转换成文件
async function convertBase64ToImage(base64Str) {
  const filePath = getDataPath('files/images')
  const imageInfo = await base64ToImage(base64Str, filePath)
  return imageInfo
}

module.exports = { convertBase64ToImage }
