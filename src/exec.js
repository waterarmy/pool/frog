const { app } = require('electron')
const { exec } = require('child_process')
const path = require('path')

module.exports = function (command) {
  console.log('> exec command\n', command)
  exec(
    command,
    {
      cwd: app.getAppPath(),
      encoding: null,
    },
    (error, stdout, stderr) => {
      if (error) {
        console.log('> error\n', error)
      }
      console.log('> stdout\n', stdout.toString())
    }
  )
}
