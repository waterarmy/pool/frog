// Modules to control application life and create native browser window
const { app, BrowserWindow, ipcMain: ipc, desktopCapturer } = require('electron')
const path = require('path')
const exec = require('./exec')
const spawn = require('./spawn')
const { convertBase64ToImage } = require('./file')

const doings = new Map()

function createWindow() {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 1600,
    height: 900,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
    },
  })

  // and load the index.html of the app.
  mainWindow.loadFile(path.join(__dirname, 'index.html'))

  // Open the DevTools.
  mainWindow.webContents.openDevTools()
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  createWindow()

  // Ipc Events
  ipc.on('do', (e, data) => {
    const { id, command } = data
    const sp = spawn(command)
    doings.set(id, sp)
    sp.stdout.on('data', (data) => {
      console.log('> stdout\n', data.toString())
      e.sender.send('do-stdout', {
        id,
        command,
        out: data.toString(),
      })
    })
    sp.stderr.on('data', (data) => {
      console.log('> stderr\n', data.toString())
    })
    sp.on('close', (code) => {
      console.log('> close', code)
      e.sender.send('done', {
        id,
        command,
        code,
      })
    })
  })

  ipc.on('undo', (e, data) => {
    const { id, command } = data
    console.log('> undo', id)
    const sp = doings.get(id)
    if (sp) {
      if (sp.exitCode === null) {
        sp.kill()
      } else {
        e.sender.send('done', {
          id,
          command,
          code: sp.exitCode,
        })
      }
    }
  })

  ipc.handle('full-screen-shot', () => {
    return desktopCapturer.getSources({ types: ['window', 'screen'] }).then(async (sources) => {
      let source
      for (source of sources) {
        console.log(source.name)
        // Filter: main screen
        if (source.name === 'Entire Screen') {
          break
        }
      }
      return source.id
    })
  })

  ipc.handle('convert-base64-to-image', (e, base64Str) => {
    return convertBase64ToImage(base64Str)
  })

  app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
