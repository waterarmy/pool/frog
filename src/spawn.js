const { app } = require('electron')
const { spawn } = require('child_process')
const path = require('path')

module.exports = function (command) {
  const args = command.split(/\s+/)
  const com = args.shift()
  console.log('> spawn command\n', com, args)
  return spawn(com, args, {
    cwd: app.getAppPath(),
  })
}
