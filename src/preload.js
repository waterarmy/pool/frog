const { contextBridge: bridge, ipcRenderer: ipc } = require('electron')

/**
 * Create a screenshot of the entire screen using the desktopCapturer module of Electron.
 *
 * @param imageFormat {String} Format of the image to generate ('image/jpeg' or 'image/png')
 **/
async function fullScreenShot() {
  let imageFormat = 'image/png'

  const handleStream = (stream) => {
    return new Promise((resolve, reject) => {
      // Create hidden video tag
      const video = document.createElement('video')
      video.style.cssText = 'position:absolute;top:-10000px;left:-10000px;'

      // Event connected to stream
      video.onloadedmetadata = function () {
        // Set video ORIGINAL height (screenshot)
        video.style.height = this.videoHeight + 'px' // videoHeight
        video.style.width = this.videoWidth + 'px' // videoWidth

        video.play()

        // Create canvas
        const canvas = document.createElement('canvas')
        canvas.width = this.videoWidth
        canvas.height = this.videoHeight
        const ctx = canvas.getContext('2d')
        // Draw video on canvas
        ctx.drawImage(video, 0, 0, canvas.width, canvas.height)

        // Remove hidden video tag
        video.remove()
        try {
          // Destroy connect to stream
          stream.getTracks()[0].stop()
        } catch (e) {}
        resolve(canvas.toDataURL(imageFormat))
      }

      video.srcObject = stream
      document.body.appendChild(video)
    })
  }

  const handleError = function (e) {
    console.log(e)
  }

  try {
    const id = await ipc.invoke('full-screen-shot')
    console.log('id', id)
    const stream = await navigator.mediaDevices.getUserMedia({
      audio: false,
      video: {
        mandatory: {
          chromeMediaSource: 'desktop',
          chromeMediaSourceId: id,
          minWidth: 1280,
          maxWidth: 4000,
          minHeight: 720,
          maxHeight: 4000,
        },
      },
    })
    const base64Str = await handleStream(stream)
    const imageInfo = await ipc.invoke('convert-base64-to-image', base64Str)
    return imageInfo
  } catch (e) {
    handleError(e)
  }
}

bridge.exposeInMainWorld('ipc', {
  send: (channel, data) => ipc.send(channel, data),
  on: (channel, fun) => ipc.on(channel, fun),
})

bridge.exposeInMainWorld('fullScreenShot', fullScreenShot)
